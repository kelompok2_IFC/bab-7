//Menghitung Jarak Hari pada Kedua Tanggal
//Header
#include <conio.h>
#include <iostream>
using namespace std;

typedef struct
{
    // DEKLARASI //
    int dd, mm, yy;
    int dd2, mm2, yy2;
    int dd3, mm3, yy3;
} Tanggal;

main()
{
    Tanggal tgl;
    //Variabel
    int total_hari1, total_hari2, total;
   
    //Inputan
    cout<<"Menghitung Jarak Hari pada Kedua Tanggal"<<endl;
    cout << "Tanggal Sebelumnya    : ";
    cin >> tgl.dd;
    cout << "Bulan Sebelumnya      : ";
    cin >> tgl.mm;
    cout << "Tahun Sebelumnya      : ";
    cin >> tgl.yy;
    cout << endl;
    cout << "Tanggal Sekarang : ";
    cin >> tgl.dd2;
    cout << "Bulan Sekarang   : ";
    cin >> tgl.mm2;
    cout << "Tahun Sekarang   : ";
    cin >> tgl.yy2;
    cout <<endl<<endl;

    //Proses 1
    total_hari1 = (tgl.yy*365) + (tgl.mm*30) + tgl.dd;
    total_hari2 = (tgl.yy2*365) + (tgl.mm2*30) + tgl.dd2;
   
    //Pemilihan
    if(total_hari2 > total_hari1){
        total = total_hari2 - total_hari1;
        cout<<total<<endl;
        //Proses 2
        //Mencari jumlah tahun dan sisa hari
            tgl.yy3=total/365;
            total=total%365;
        //Mencari jumlah bulan dan sisa hari
            tgl.mm3=total/30;
            tgl.dd3=total%30;
        //Output
        cout<<tgl.yy3<<" Tahun "<<tgl.mm3<<" Bulan "<<tgl.dd3<<" Hari "<<endl;
    }else
        cout <<"Inputan Salah";
   
    getch();
}
